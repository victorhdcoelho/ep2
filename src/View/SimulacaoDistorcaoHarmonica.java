package View;
import java.awt.EventQueue;
import Model.DistorcaoHarmonicaHarmonicos;
import Model.DistorcaoHarmonicaResultados;
import Model.FluxoDePotenciaTensao;
import Model.GraphPanel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SpinnerNumberModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SimulacaoDistorcaoHarmonica extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField Amplitude;
	private JTextField AmplitudeHarmonica1;
	private JTextField amplitudeHarmonica2;
	private JTextField AmplitudeHarmonica3;
	private JTextField AmplitudeHarmonica4;
	private JTextField AmplitudeHarmonica5;
	private JTextField AmplitudeHarmonica6;
	protected int ordem;
	protected List<Double>list1=new ArrayList<Double>();
	protected List<Double>list2=new ArrayList<Double>();
	protected List<Double>list3=new ArrayList<Double>();
	protected List<Double>list4=new ArrayList<Double>();
	protected List<Double>list5=new ArrayList<Double>();
	protected List<Double>list6=new ArrayList<Double>();
	protected List<Double>fundamental;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimulacaoDistorcaoHarmonica frame = new SimulacaoDistorcaoHarmonica();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SimulacaoDistorcaoHarmonica() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1400, 900);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelComponenteFundamental = new JPanel();
		panelComponenteFundamental.setBorder(new TitledBorder(null, "Componente Fundamental", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelComponenteFundamental.setBounds(12, 12, 694, 187);
		contentPane.add(panelComponenteFundamental);
		panelComponenteFundamental.setLayout(null);
		
		JSpinner spinnerOrdem = new JSpinner();
		spinnerOrdem.setModel(new SpinnerNumberModel(new Integer(0), null, null, new Integer(1)));
		spinnerOrdem.setBounds(926, 54, 64, 22);
		contentPane.add(spinnerOrdem);
		
		JPanel panelInternoDocompoeneteFundamental = new JPanel();
		panelInternoDocompoeneteFundamental.setBounds(5, 19, 684, 163);
		panelComponenteFundamental.add(panelInternoDocompoeneteFundamental);
		panelInternoDocompoeneteFundamental.setLayout(null);
		
		List<Double>list = new ArrayList<Double>();
		FluxoDePotenciaTensao fTensao= new FluxoDePotenciaTensao(0,0);
		list= fTensao.calcularSimulacao(0);
		GraphPanel graphPanel = new GraphPanel(list);
		graphPanel.setBounds(10, 0, 674, 163);
		panelInternoDocompoeneteFundamental.add(graphPanel);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, "Resultado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_7.setBounds(12, 529, 694, 150);
		contentPane.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(12, 21, 682, 117);
		panel_7.add(panel_3);
		panel_3.setLayout(null);
		
		List<Double>list1111111 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH111111 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list1111111=DHH111111.calcularSimulacao();
		GraphPanel graphPanel_7 = new GraphPanel(list1111111);
		graphPanel_7.setBounds(0, 0, 682, 117);
		panel_3.add(graphPanel_7);
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setBounds(724, 38, 76, 17);
		contentPane.add(lblAmplitude);
		
		Amplitude = new JTextField();
		Amplitude.setBounds(718, 54, 82, 21);
		contentPane.add(Amplitude);
		Amplitude.setColumns(10);
		
		JSpinner spinnerAnguloDeFaseComponenteFundamental = new JSpinner();
		spinnerAnguloDeFaseComponenteFundamental.setModel(new SpinnerNumberModel(0.0, -180.0, 180.0, 1.0));
		spinnerAnguloDeFaseComponenteFundamental.setBounds(718, 107, 244, 22);
		contentPane.add(spinnerAnguloDeFaseComponenteFundamental);
		
		JLabel label_1 = new JLabel("Angulo de Fase [0°]");
		label_1.setBounds(724, 78, 127, 17);
		contentPane.add(label_1);
		
		JLabel lblOrdem = new JLabel("Ordem");
		lblOrdem.setBounds(926, 38, 64, 17);
		contentPane.add(lblOrdem);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setFont(new Font("Dialog", Font.BOLD, 20));
		lblResultado.setBounds(724, 529, 101, 39);
		contentPane.add(lblResultado);
		
		JLabel lblSerieEFornier = new JLabel("Série de Fourier da onda resultante");
		lblSerieEFornier.setBounds(829, 583, 244, 17);
		contentPane.add(lblSerieEFornier);
		
		JLabel lblNewLabel = new JLabel("f ( t )=220 cos ( ωt+ 0° ) +20 cos ( 3 ωt +30 ° ) +15 cos (5 ωt−90°)");
		lblNewLabel.setBounds(757, 624, 419, 17);
		contentPane.add(lblNewLabel);
		
		JLabel lblHarmonicos1 = new JLabel("Número de harmonicos");
		lblHarmonicos1.setBounds(454, 211, 158, 17);
		contentPane.add(lblHarmonicos1);
	
		
		JLabel Amplitude1 = new JLabel("Amplitude");
		Amplitude1.setBounds(624, 211, 76, 17);
		contentPane.add(Amplitude1);
		
		JLabel lblAnguloDeFase1 = new JLabel("Angulo de Fase [0°]");
		lblAnguloDeFase1.setBounds(724, 211, 127, 17);
		contentPane.add(lblAnguloDeFase1);
		
		JSpinner spinnerHarmonica1 = new JSpinner();
		spinnerHarmonica1.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica1.setEnabled(false);
		spinnerHarmonica1.setBounds(454, 233, 158, 22);
		contentPane.add(spinnerHarmonica1);
		
		JSpinner AnguloHarmonica1 = new JSpinner();
		AnguloHarmonica1.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica1.setEnabled(false);
		AnguloHarmonica1.setBounds(724, 233, 127, 22);
		contentPane.add(AnguloHarmonica1);
		
		JPanel Harmonica1 = new JPanel();
		Harmonica1.setBounds(12, 202, 442, 83);
		contentPane.add(Harmonica1);
		Harmonica1.setBorder(new TitledBorder(null, "Harmonicas1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Harmonica1.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 12, 430, 71);
		Harmonica1.add(panel_1);
		panel_1.setLayout(null);
		
		List<Double>list1_1 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list1_1=DHH.calcularSimulacao();
		GraphPanel graphPanel_1 = new GraphPanel(list1_1);
		graphPanel_1.setBounds(0, 0, 430, 71);
		panel_1.add(graphPanel_1);
		
		AmplitudeHarmonica1 = new JTextField();
		AmplitudeHarmonica1.setEditable(false);
		AmplitudeHarmonica1.setEnabled(false);
		AmplitudeHarmonica1.setColumns(10);
		AmplitudeHarmonica1.setBounds(624, 233, 82, 21);
		contentPane.add(AmplitudeHarmonica1);
		
		JLabel lblHarmonicos2 = new JLabel("Número de harmonicos");
		lblHarmonicos2.setBounds(454, 294, 158, 17);
		contentPane.add(lblHarmonicos2);
		
		JLabel Amplitude2 = new JLabel("Amplitude");
		Amplitude2.setBounds(624, 294, 76, 17);
		contentPane.add(Amplitude2);
		
		
		JLabel AnguloDeFase2 = new JLabel("Angulo de Fase [0°]");
		AnguloDeFase2.setBounds(724, 294, 127, 17);
		contentPane.add(AnguloDeFase2);
		
		amplitudeHarmonica2 = new JTextField();
		amplitudeHarmonica2.setEnabled(false);
		amplitudeHarmonica2.setEditable(false);
		amplitudeHarmonica2.setColumns(10);
		amplitudeHarmonica2.setBounds(624, 310, 82, 21);
		contentPane.add(amplitudeHarmonica2);
		
		JPanel Harmonica2 = new JPanel();
		Harmonica2.setBorder(new TitledBorder(null, "Harmonicas2", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Harmonica2.setBounds(12, 283, 442, 82);
		contentPane.add(Harmonica2);
		Harmonica2.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(12, 19, 430, 51);
		Harmonica2.add(panel_2);
		panel_2.setLayout(null);
		
		List<Double>list11 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH1 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list11=DHH1.calcularSimulacao();
		GraphPanel graphPanel_2 = new GraphPanel(list11);
		graphPanel_2.setBounds(0, 0, 430, 50);
		panel_2.add(graphPanel_2);
		
		JSpinner spinnerHarmonica2 = new JSpinner();
		spinnerHarmonica2.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica2.setEnabled(false);
		spinnerHarmonica2.setBounds(454, 310, 158, 22);
		contentPane.add(spinnerHarmonica2);
		
		JSpinner AnguloHarmonica2 = new JSpinner();
		AnguloHarmonica2.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica2.setEnabled(false);
		AnguloHarmonica2.setBounds(724, 310, 127, 22);
		contentPane.add(AnguloHarmonica2);
		
		JPanel Harmonica3 = new JPanel();
		Harmonica3.setLayout(null);
		Harmonica3.setBorder(new TitledBorder(null, "Harmonicas3", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Harmonica3.setBounds(12, 366, 442, 82);
		contentPane.add(Harmonica3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(12, 19, 430, 51);
		Harmonica3.add(panel_4);
		panel_4.setLayout(null);
		
		List<Double>list111 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH11 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list111=DHH11.calcularSimulacao();
		GraphPanel graphPanel_3 = new GraphPanel(list111);
		graphPanel_3.setBounds(0, 0, 430, 50);
		panel_4.add(graphPanel_3);
		
		JLabel label_5 = new JLabel("Número de harmonicos");
		label_5.setBounds(454, 378, 158, 17);
		contentPane.add(label_5);
		
		JSpinner spinnerHarmonica3 = new JSpinner();
		spinnerHarmonica3.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica3.setEnabled(false);
		spinnerHarmonica3.setBounds(454, 392, 158, 22);
		contentPane.add(spinnerHarmonica3);
		
		JLabel label_6 = new JLabel("Amplitude");
		label_6.setBounds(624, 378, 76, 17);
		contentPane.add(label_6);
		
		AmplitudeHarmonica3 = new JTextField();
		AmplitudeHarmonica3.setEditable(false);
		AmplitudeHarmonica3.setEnabled(false);
		AmplitudeHarmonica3.setColumns(10);
		AmplitudeHarmonica3.setBounds(624, 392, 82, 21);
		contentPane.add(AmplitudeHarmonica3);
		
		JLabel label_7 = new JLabel("Angulo de Fase [0°]");
		label_7.setBounds(724, 378, 127, 17);
		contentPane.add(label_7);
		
		JSpinner AnguloHarmonica3 = new JSpinner();
		AnguloHarmonica3.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica3.setEnabled(false);
		AnguloHarmonica3.setBounds(724, 392, 127, 22);
		contentPane.add(AnguloHarmonica3);
		
		JPanel Harmonica4 = new JPanel();
		Harmonica4.setLayout(null);
		Harmonica4.setBorder(new TitledBorder(null, "Harmonicas4", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Harmonica4.setBounds(12, 447, 442, 82);
		contentPane.add(Harmonica4);
		
		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBounds(12, 19, 430, 51);
		Harmonica4.add(panel_9);
		
		List<Double>list1111 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH111 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list1111=DHH111.calcularSimulacao();
		GraphPanel graphPanel_4 = new GraphPanel(list1111);
		graphPanel_4.setBounds(0, 0, 430, 50);
		panel_9.add(graphPanel_4);
		
		JLabel label_8 = new JLabel("Número de harmonicos");
		label_8.setBounds(454, 457, 158, 17);
		contentPane.add(label_8);
		
		JSpinner spinnerHarmonica4 = new JSpinner();
		spinnerHarmonica4.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica4.setEnabled(false);
		spinnerHarmonica4.setBounds(454, 472, 158, 22);
		contentPane.add(spinnerHarmonica4);
		
		JLabel label_9 = new JLabel("Amplitude");
		label_9.setBounds(624, 457, 76, 17);
		contentPane.add(label_9);
		
		AmplitudeHarmonica4 = new JTextField();
		AmplitudeHarmonica4.setEnabled(false);
		AmplitudeHarmonica4.setEditable(false);
		AmplitudeHarmonica4.setColumns(10);
		AmplitudeHarmonica4.setBounds(624, 472, 82, 21);
		contentPane.add(AmplitudeHarmonica4);
		
		JLabel label_10 = new JLabel("Angulo de Fase [0°]");
		label_10.setBounds(724, 457, 127, 17);
		contentPane.add(label_10);
		
		JSpinner AnguloHarmonica4 = new JSpinner();
		AnguloHarmonica4.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica4.setEnabled(false);
		AnguloHarmonica4.setBounds(724, 472, 127, 22);
		contentPane.add(AnguloHarmonica4);
		
		JPanel Harmonica5 = new JPanel();
		Harmonica5.setLayout(null);
		Harmonica5.setBorder(new TitledBorder(null, "Harmonicas5", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Harmonica5.setBounds(886, 190, 442, 95);
		contentPane.add(Harmonica5);
		
		JPanel panel_11 = new JPanel();
		panel_11.setLayout(null);
		panel_11.setBounds(12, 12, 430, 59);
		Harmonica5.add(panel_11);
		
		List<Double>list11111 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH1111 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list11111=DHH1111.calcularSimulacao();
		GraphPanel graphPanel_5 = new GraphPanel(list11111);
		graphPanel_5.setBounds(0, 0, 430, 59);
		panel_11.add(graphPanel_5);
		
		AmplitudeHarmonica5 = new JTextField();
		AmplitudeHarmonica5.setEditable(false);
		AmplitudeHarmonica5.setEnabled(false);
		AmplitudeHarmonica5.setColumns(10);
		AmplitudeHarmonica5.setBounds(1094, 310, 82, 21);
		contentPane.add(AmplitudeHarmonica5);
		
		JSpinner spinnerHarmonica5 = new JSpinner();
		spinnerHarmonica5.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica5.setEnabled(false);
		spinnerHarmonica5.setBounds(915, 310, 158, 22);
		contentPane.add(spinnerHarmonica5);
		JLabel label_14 = new JLabel("Amplitude");
		
		label_14.setBounds(1100, 283, 76, 17);
		contentPane.add(label_14);
		
		JSpinner AnguloHarmonica5 = new JSpinner();
		AnguloHarmonica5.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica5.setEnabled(false);
		AnguloHarmonica5.setBounds(1188, 310, 127, 22);
		contentPane.add(AnguloHarmonica5);
		
		JLabel NumOfH5 = new JLabel("Número de harmonicos");
		NumOfH5.setBounds(915, 283, 158, 17);
		contentPane.add(NumOfH5);
		
		
		JLabel AngH5 = new JLabel("Angulo de Fase [0°]");
		AngH5.setBounds(1188, 283, 127, 17);
		contentPane.add(AngH5);
		
		JSpinner AnguloHarmonica6 = new JSpinner();
		AnguloHarmonica6.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		AnguloHarmonica6.setEnabled(false);
		AnguloHarmonica6.setBounds(1201, 472, 127, 22);
		contentPane.add(AnguloHarmonica6);
		
		JPanel panel_12 = new JPanel();
		panel_12.setLayout(null);
		panel_12.setBorder(new TitledBorder(null, "Harmonicas6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_12.setBounds(886, 354, 442, 95);
		contentPane.add(panel_12);
		
		JPanel Harmonica6 = new JPanel();
		Harmonica6.setLayout(null);
		Harmonica6.setBounds(12, 24, 430, 59);
		panel_12.add(Harmonica6);
		
		List<Double>list111111 = new ArrayList<Double>();
		DistorcaoHarmonicaHarmonicos DHH11111 =new DistorcaoHarmonicaHarmonicos(0.0,0.0,0.0);
		list111111=DHH11111.calcularSimulacao();
		GraphPanel graphPanel_6 = new GraphPanel(list111111);
		graphPanel_6.setBounds(0, 0, 430, 59);
		Harmonica6.add(graphPanel_6);
		
		JSpinner spinnerHarmonica6 = new JSpinner();
		spinnerHarmonica6.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerHarmonica6.setEnabled(false);
		spinnerHarmonica6.setBounds(896, 472, 158, 22);
		contentPane.add(spinnerHarmonica6);
		
		AmplitudeHarmonica6 = new JTextField();
		AmplitudeHarmonica6.setEditable(false);
		AmplitudeHarmonica6.setEnabled(false);
		AmplitudeHarmonica6.setColumns(10);
		AmplitudeHarmonica6.setBounds(1100, 472, 82, 21);
		contentPane.add(AmplitudeHarmonica6);
		JLabel NumOfH6_16 = new JLabel("Número de harmonicos");
		NumOfH6_16.setBounds(896, 447, 158, 17);
		contentPane.add(NumOfH6_16);
		
		
		JLabel AmOfH6 = new JLabel("Amplitude");
		AmOfH6.setBounds(1106, 447, 76, 17);
		contentPane.add(AmOfH6);
		
		JLabel AngOfH6 = new JLabel("Angulo de Fase [0°]");
		AngOfH6.setBounds(1201, 447, 127, 17);
		contentPane.add(AngOfH6);
		
		JButton btnValidar = new JButton("Validar");
		btnValidar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Double> list= new ArrayList<Double>();
				double amplitude;
				amplitude = Double.parseDouble(Amplitude.getText());
				double anguloDeFase;
				anguloDeFase= (double)spinnerAnguloDeFaseComponenteFundamental.getValue();
				FluxoDePotenciaTensao fTensao= new FluxoDePotenciaTensao(amplitude,anguloDeFase);
				list=fTensao.calcularSimulacao(0);
				fundamental=list;
				graphPanel.setScores(list);
				ordem= (int) spinnerOrdem.getValue();
				if(ordem>=1) {
					spinnerHarmonica1.setEnabled(true);
					AnguloHarmonica1.setEnabled(true);
					AmplitudeHarmonica1.setEditable(true);
					AmplitudeHarmonica1.setEnabled(true);
					if(ordem >= 2) {
						spinnerHarmonica2.setEnabled(true);
						AnguloHarmonica2.setEnabled(true);
						amplitudeHarmonica2.setEnabled(true);
						amplitudeHarmonica2.setEditable(true);
						if(ordem>=3) {
							AmplitudeHarmonica3.setEditable(true);
							AmplitudeHarmonica3.setEnabled(true);
							spinnerHarmonica3.setEnabled(true);
							AnguloHarmonica3.setEnabled(true);
							if(ordem>=4) {
								AmplitudeHarmonica4.setEditable(true);
								AmplitudeHarmonica4.setEnabled(true);
								spinnerHarmonica4.setEnabled(true);
								AnguloHarmonica4.setEnabled(true);
								if(ordem>=5) {
									AmplitudeHarmonica5.setEditable(true);
									AmplitudeHarmonica5.setEnabled(true);
									spinnerHarmonica5.setEnabled(true);
									AnguloHarmonica5.setEnabled(true);
									if(ordem == 6) {
										AmplitudeHarmonica6.setEditable(true);
										AmplitudeHarmonica6.setEnabled(true);
										spinnerHarmonica6.setEnabled(true);
										AnguloHarmonica6.setEnabled(true);
									}
								}
							}
						}
						
					}


				}
			}
		});
		
		
		
		btnValidar.setBounds(1009, 104, 108, 27);
		contentPane.add(btnValidar);
		
		JButton btnValidar_1 = new JButton("Validar");
		btnValidar_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				for(int i=0;i<100;i++) {
					list1.add(0.0);
					list2.add(0.0);
					list3.add(0.0);
					list4.add(0.0);
					list5.add(0.0);
					list6.add(0.0);
				}
				if(ordem >=1) {
				double amplitude;
				amplitude=Double.parseDouble(AmplitudeHarmonica1.getText());
			 double	anguloDeFase;
			 	anguloDeFase=(double) spinnerHarmonica1.getValue();
			 double NH;
			 	NH=(double) spinnerHarmonica1.getValue();
					List<Double>H1;
					DistorcaoHarmonicaHarmonicos DHH =new DistorcaoHarmonicaHarmonicos(amplitude,anguloDeFase,NH);
					H1=DHH.calcularSimulacao();
					list1 = H1;
					graphPanel_1.setScores(H1);
					if(ordem>=2) {
					 double amplitude2;
						amplitude2=Double.parseDouble(amplitudeHarmonica2.getText());
					 double	anguloDeFase2;
					 	anguloDeFase2=(double) spinnerHarmonica2.getValue();
					 double NH2;
					 NH2=(double) spinnerHarmonica2.getValue();
						List<Double>H11;
						DistorcaoHarmonicaHarmonicos DHH1 =new DistorcaoHarmonicaHarmonicos(amplitude2,anguloDeFase2,NH2);
						H11=DHH1.calcularSimulacao();
						list2=H11;
						graphPanel_2.setScores(H11);
						if(ordem>=3) {
							 double amplitude3;
								amplitude3=Double.parseDouble(AmplitudeHarmonica3.getText());
							 double	anguloDeFase3;
							 	anguloDeFase3=(double) spinnerHarmonica3.getValue();
							 double NH3;
							 NH3=(double) spinnerHarmonica3.getValue();
								List<Double>H111;
								DistorcaoHarmonicaHarmonicos DHH11 =new DistorcaoHarmonicaHarmonicos(amplitude3,anguloDeFase3,NH3);
								H111=DHH11.calcularSimulacao();
								list3=H111;
								graphPanel_3.setScores(H111);
								if(ordem >= 4) {
									 double amplitude4;
										amplitude4=Double.parseDouble(AmplitudeHarmonica4.getText());
									 double	anguloDeFase4;
									 	anguloDeFase4=(double) spinnerHarmonica4.getValue();
									 double NH4;
									 NH4=(double) spinnerHarmonica4.getValue();
										List<Double>H1111;
										DistorcaoHarmonicaHarmonicos DHH111 =new DistorcaoHarmonicaHarmonicos(amplitude4,anguloDeFase4,NH4);
										H1111=DHH111.calcularSimulacao();
										list4=H1111;
										graphPanel_4.setScores(H1111);
										if(ordem>=5) {
											 double amplitude5;
												amplitude5=Double.parseDouble(AmplitudeHarmonica5.getText());
											 double	anguloDeFase5;
											 	anguloDeFase5=(double) spinnerHarmonica5.getValue();
											 double NH5;
											 NH5=(double) spinnerHarmonica5.getValue();
												List<Double>H11111;
												DistorcaoHarmonicaHarmonicos DHH1111 =new DistorcaoHarmonicaHarmonicos(amplitude5,anguloDeFase5,NH5);
												H11111=DHH1111.calcularSimulacao();
												list5=H11111;
												graphPanel_5.setScores(H11111);
											if(ordem == 6) {
												 double amplitude6;
													amplitude6=Double.parseDouble(AmplitudeHarmonica6.getText());
												 double	anguloDeFase6;
												 	anguloDeFase6=(double) spinnerHarmonica6.getValue();
												 double NH6;
												 NH6=(double) spinnerHarmonica5.getValue();
													List<Double>H111111;
													DistorcaoHarmonicaHarmonicos DHH11111 =new DistorcaoHarmonicaHarmonicos(amplitude6,anguloDeFase6,NH6);
													H111111=DHH11111.calcularSimulacao();
													list6=H111111;
													graphPanel_6.setScores(H111111);
											}
										}
									
								}
							
						}
					}
				}
			}
		});
		btnValidar_1.setBounds(1220, 506, 108, 27);
		contentPane.add(btnValidar_1);
		
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPrincipal menu;
				menu= new MenuPrincipal();
				menu.setVisible(true);
			}
		});
		btnVoltar.setBounds(1220, 583, 108, 63);
		contentPane.add(btnVoltar);
		
		JButton btnSimular = new JButton("Simular");
		btnSimular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Double>resultado;
				DistorcaoHarmonicaResultados resultado1 = new DistorcaoHarmonicaResultados(list1,list2,list3,list4,list5,list6);
				resultado=resultado1.calcularSimulacao(fundamental);
				graphPanel_7.setScores(resultado);
			}
		});
		btnSimular.setBounds(1079, 585, 108, 27);
		contentPane.add(btnSimular);
	}
}
