package View;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class MenuPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setFont(new Font("Dialog", Font.ITALIC, 12));
		setBackground(UIManager.getColor("Button.shadow"));
		setTitle("Menu Principal de simulação ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 293);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setFont(new Font("Dialog", Font.BOLD, 25));
		lblMenu.setBounds(172, 12, 84, 35);
		contentPane.add(lblMenu);
		
		JButton btnFPF = new JButton("Simulação de Fluxo de Potência Fundamental.");
		btnFPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			SimulacaoDeFluxoDePotencia.main(null);
			setVisible(false);
			}
		});
		btnFPF.setBounds(59, 66, 329, 27);
		contentPane.add(btnFPF);
		
		JButton buttonDH = new JButton("Simulação de Distorção Harmonica.");
		buttonDH.setBounds(59, 129, 329, 27);
		contentPane.add(buttonDH);
		buttonDH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			SimulacaoDistorcaoHarmonica.main(null);
			setVisible(false);
			}
		});
		
		JButton btnEncerrar = new JButton("Encerrar");
		btnEncerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 MenuAlerta.main(null);
				
			}
		});
		btnEncerrar.setBounds(59, 183, 329, 55);
		contentPane.add(btnEncerrar);
	}
}
