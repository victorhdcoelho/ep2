package View;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import Model.FluxoDePotenciaCorrente;
import Model.FluxoDePotenciaResultado;
import Model.FluxoDePotenciaTensao;
import Model.GraphPanel;

import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JTextPane;

public class SimulacaoDeFluxoDePotencia extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textFieldFatorDePotencia;
	private JTextField textFieldPotenciaAparente;
	private JTextField textFieldPotenciaReativa;
	private JTextField textFieldPotenciaAtiva;
	private JTextField textFieldAmplitude;
	private JTextField textFielAmplitudeCorrente;
	protected List<Double> listTensao;
	protected List<Double> listCorrente;
	//Variaveis globais dentro da classe para que possa ser calculado com mais facilidade os resultados finais
	//Aeff = Amplitude de Corrente
	//Veff = Amplitude de Tensao
	protected double Aeff;
	protected double Veff;
	protected double anguloTensao;
	protected double anguloCorrente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimulacaoDeFluxoDePotencia frame = new SimulacaoDeFluxoDePotencia();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SimulacaoDeFluxoDePotencia() {
		setBackground(UIManager.getColor("Button.shadow"));
		setTitle("Simulação de Fluxo de Tensão Fundamental");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 867, 567);
		getContentPane().setLayout(null);
        final int red=238;
        final int green=238;
        final int blue=238;
        Color mycolor = new Color(red,green,blue);
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(mycolor);
		panel_2.setForeground(Color.WHITE);
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Tens\u00E3o", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel_2.setBounds(7, 0, 382, 145);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		FluxoDePotenciaTensao fluxoDePotencia =  new FluxoDePotenciaTensao(0,0);
		List <Double> list = new ArrayList<>();
		list = fluxoDePotencia.calcularSimulacao(0);
		GraphPanel graphPanel = new GraphPanel(list);
		graphPanel.setBorder(null);
		graphPanel.setBackground(SystemColor.controlHighlight);
		graphPanel.setForeground(Color.WHITE);
		graphPanel.setBounds(12, 23, 358, 110);
		panel_2.add(graphPanel);
	
		JSpinner spinnerAnguloDeFaseTensao = new JSpinner();
		spinnerAnguloDeFaseTensao.setModel(new SpinnerNumberModel(0.0, -180.0, 180.0, 1.0));
		spinnerAnguloDeFaseTensao.setBounds(583, 58, 164, 22);
		getContentPane().add(spinnerAnguloDeFaseTensao);
		
		textFieldAmplitude = new JTextField();
		textFieldAmplitude.setBounds(453, 47, 114, 21);
		getContentPane().add(textFieldAmplitude);
		textFieldAmplitude.setColumns(10);
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setBounds(457, 32, 70, 17);
		getContentPane().add(lblAmplitude);
		textField = new JTextField();
		textField.setColumns(10);
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		JTextPane textPaneInvalidaAmplitudeTensao = new JTextPane();
		textPaneInvalidaAmplitudeTensao.setEditable(false);
		textPaneInvalidaAmplitudeTensao.setBackground(mycolor);
		textPaneInvalidaAmplitudeTensao.setBounds(401, 76, 170, 41);
		getContentPane().add(textPaneInvalidaAmplitudeTensao);
		
		JTextPane textPaneInvalidaVeffTensao = new JTextPane();
		textPaneInvalidaVeffTensao.setEditable(false);
		textPaneInvalidaVeffTensao.setBackground(mycolor);
		textPaneInvalidaVeffTensao.setBounds(401, 129, 170, 41);
		getContentPane().add(textPaneInvalidaVeffTensao);
		
		JButton btnValidarTensao = new JButton("Validar");
		btnValidarTensao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			double amplitude = 0;
			double anguloDeFase=0;
			//Tratamento de erros para que não ocorra valores vazios ou listas sem itens no grafico
			try {	
				amplitude=Double.parseDouble(textFieldAmplitude.getText());
				Veff=amplitude;
				textPaneInvalidaAmplitudeTensao.setText("");
			}	catch (NumberFormatException e1) {
				textPaneInvalidaAmplitudeTensao.setText("Formato amplitude inválido!\n"
						+ "		  Por favor tente novamente! \n");
			}
			try {
				anguloDeFase=(double) spinnerAnguloDeFaseTensao.getValue();
				anguloTensao=anguloDeFase;
			}	catch (NumberFormatException e1) {
				textPaneInvalidaVeffTensao.setText("Formato angulo de fase inválido! \n"
						+ "		  Por favor tente novamente !\n");
			}			
			try {
				FluxoDePotenciaTensao fluxoDePotencia2 =  new FluxoDePotenciaTensao(amplitude,anguloDeFase);
				List <Double> list = new ArrayList<>();
				list = fluxoDePotencia2.calcularSimulacao(0);
				graphPanel.setScores(list);
				listTensao=list;
				}
			catch (NumberFormatException e1) {
				textPaneInvalidaVeffTensao.setText("Formato veff inválido! \n"
						+ "		  Por favor tente novamente !\n");
			}

		}});
	
		
		JLabel lblAnguloDeFaseTensao = new JLabel("Angulo de Fase[0°]");
		lblAnguloDeFaseTensao.setBounds(583, 32, 164, 17);
		getContentPane().add(lblAnguloDeFaseTensao);
		btnValidarTensao.setBounds(592, 113, 155, 27);
		getContentPane().add(btnValidarTensao);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Corrente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel_3.setBounds(7, 188, 382, 124);
		getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		JPanel panelCorrente = new JPanel();
		panelCorrente.setBounds(5, 19, 372, 100);
		panel_3.add(panelCorrente);
		panelCorrente.setLayout(null);
		
		textFielAmplitudeCorrente = new JTextField();
		textFielAmplitudeCorrente.setColumns(10);
		textFielAmplitudeCorrente.setBounds(453, 229, 114, 21);
		getContentPane().add(textFielAmplitudeCorrente);
		
		FluxoDePotenciaCorrente fluxoDePotencia1 =  new FluxoDePotenciaCorrente(0,0);
		List <Double> list1 = new ArrayList<>();
		list1 = fluxoDePotencia1.calcularSimulacao(0);
		GraphPanel graphPanel_1 = new GraphPanel(list1);
		graphPanel_1.setBounds(0, 0, 372, 100);
		panelCorrente.add(graphPanel_1);
		
		JSpinner spinnerAnguloDeFaseCorrente = new JSpinner();
		spinnerAnguloDeFaseCorrente.setBounds(583, 230, 164, 22);
		spinnerAnguloDeFaseCorrente.setModel(new SpinnerNumberModel(0.0, -180.0, 180.0, 1.0));
		getContentPane().add(spinnerAnguloDeFaseCorrente);
		
		JTextPane textPaneInvalidaAmplitudeCorrente = new JTextPane();
		textPaneInvalidaAmplitudeCorrente.setEditable(false);
		textPaneInvalidaAmplitudeCorrente.setBackground(UIManager.getColor("Button.background"));
		textPaneInvalidaAmplitudeCorrente.setBounds(397, 251, 170, 41);
		getContentPane().add(textPaneInvalidaAmplitudeCorrente);
		
		JTextPane textPaneInvalidaAeffCorrente = new JTextPane();
		textPaneInvalidaAeffCorrente.setEditable(false);
		textPaneInvalidaAeffCorrente.setBackground(UIManager.getColor("Button.background"));
		textPaneInvalidaAeffCorrente.setBounds(398, 301, 170, 41);
		getContentPane().add(textPaneInvalidaAeffCorrente);
	
		
		JButton btnValidarCorrente = new JButton("Validar");
		btnValidarCorrente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double amplitude = 0;
				double anguloDeFase=0;
				//Tratamento de erros para que não ocorra valores vazios ou listas sem itens no grafico
				try {	
					amplitude=Double.parseDouble(textFielAmplitudeCorrente.getText());
					textPaneInvalidaAmplitudeCorrente.setText("");
					Aeff=amplitude;
				}	catch (NumberFormatException e1) {
					textPaneInvalidaAmplitudeCorrente.setText("Formato amplitude inválido!\n"
							+ "		  Por favor tente novamente! \n");
				}
				try {
					anguloDeFase=(double) spinnerAnguloDeFaseCorrente.getValue();
					anguloCorrente = anguloDeFase;
				}	catch (NumberFormatException e1) {
					textPaneInvalidaAeffCorrente.setText("Formato angulo de fase inválido! \n"
							+ "		  Por favor tente novamente !\n");
				}			
				try {
					FluxoDePotenciaCorrente fluxoDePotencia2 =  new FluxoDePotenciaCorrente(amplitude,anguloDeFase);
					List <Double> list = new ArrayList<>();
					list = fluxoDePotencia2.calcularSimulacao(0);			
					graphPanel_1.setScores(list);
					listCorrente = list;					
					}
				catch (NumberFormatException e1) {
					textPaneInvalidaAeffCorrente.setText("Formato Aeff inválido! \n"
							+ "		  Por favor tente novamente !\n");
				}
				
			}
		});
		btnValidarCorrente.setBounds(592, 280, 155, 27);
		getContentPane().add(btnValidarCorrente);
		
		JLabel labelAnguloDeFaseCorrente = new JLabel("Angulo de Fase [0°]");
		labelAnguloDeFaseCorrente.setBounds(583, 201, 164, 17);
		getContentPane().add(labelAnguloDeFaseCorrente);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setFont(new Font("Dialog", Font.BOLD, 18));
		lblResultado.setBounds(413, 342, 114, 27);
		getContentPane().add(lblResultado);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Pot\u00EAncia Instant\u00E2nea", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel_4.setBounds(7, 342, 382, 124);
		getContentPane().add(panel_4);
		panel_4.setLayout(null);
		
		JPanel panelPotenciaInstantanea = new JPanel();
		panelPotenciaInstantanea.setBounds(5, 19, 372, 100);
		panel_4.add(panelPotenciaInstantanea);
		panelPotenciaInstantanea.setLayout(null);
		
		List<Double> resultado = new ArrayList<Double>();
		for(int i=0;i<100;i++) {
			resultado.add(0.0);
		}
		FluxoDePotenciaResultado fluxoResultado = new FluxoDePotenciaResultado();
		fluxoResultado.setListCorrente(listCorrente);
		fluxoResultado.setListTensao(listTensao);	
		GraphPanel graphPanel_2 = new GraphPanel(resultado);
		graphPanel_2.setBounds(0, 10, 372, 90);
		panelPotenciaInstantanea.add(graphPanel_2);
		
		JPanel panelTriangulo = new JPanel();
		panelTriangulo.setBounds(583, 361, 164, 129);
		getContentPane().add(panelTriangulo);
		
		textFieldFatorDePotencia = new JTextField();
		textFieldFatorDePotencia.setEditable(false);
		textFieldFatorDePotencia.setBounds(413, 506, 135, 21);
		getContentPane().add(textFieldFatorDePotencia);
		textFieldFatorDePotencia.setColumns(10);
		
		textFieldPotenciaAparente = new JTextField();
		textFieldPotenciaAparente.setEditable(false);
		textFieldPotenciaAparente.setBounds(275, 506, 135, 21);
		getContentPane().add(textFieldPotenciaAparente);
		textFieldPotenciaAparente.setColumns(10);
		
		textFieldPotenciaReativa = new JTextField();
		textFieldPotenciaReativa.setEditable(false);
		textFieldPotenciaReativa.setBounds(141, 506, 124, 21);
		getContentPane().add(textFieldPotenciaReativa);
		textFieldPotenciaReativa.setColumns(10);
		
		textFieldPotenciaAtiva = new JTextField();
		textFieldPotenciaAtiva.setEditable(false);
		textFieldPotenciaAtiva.setBounds(12, 506, 125, 21);
		getContentPane().add(textFieldPotenciaAtiva);
		textFieldPotenciaAtiva.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Potência Ativa");
		lblNewLabel.setBounds(22, 484, 104, 17);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Potência Reativa");
		lblNewLabel_1.setBounds(141, 484, 111, 17);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblPotncia = new JLabel("Potência Aparente");
		lblPotncia.setBounds(264, 484, 125, 17);
		getContentPane().add(lblPotncia);
		
		JLabel lblFatorDePotncia = new JLabel("Fator de Potência");
		lblFatorDePotncia.setBounds(413, 484, 114, 17);
		getContentPane().add(lblFatorDePotncia);
		
		JLabel lblPotnciaInsta = new JLabel("Potência Instantânea");
		lblPotnciaInsta.setBounds(138, 343, 135, 17);
		getContentPane().add(lblPotnciaInsta);
		
		JLabel lblTriangulo = new JLabel("Triângulo de Potência");
		lblTriangulo.setBounds(583, 343, 152, 17);
		getContentPane().add(lblTriangulo);
		
		JButton btnSimular = new JButton("Simular");
		btnSimular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Double> resultado;
				FluxoDePotenciaResultado fluxoResultado = new FluxoDePotenciaResultado();
				fluxoResultado.setListCorrente(listCorrente);
				fluxoResultado.setListTensao(listTensao);
				resultado = fluxoResultado.calcularSimulacao();
				graphPanel_2.setScores(resultado);
				
				double potenciaAtiva = Veff*Aeff*Math.cos(Math.toRadians(anguloTensao-anguloCorrente));
				textFieldPotenciaAtiva.setText(String.valueOf(potenciaAtiva));
				
				double potenciaReativa =Veff*Aeff*Math.sin(Math.toRadians(anguloTensao-anguloCorrente));
				textFieldPotenciaReativa.setText(String.valueOf(potenciaReativa));
				
				double potenciaAparente = Veff * Aeff;
				textFieldPotenciaAparente.setText(String.valueOf(potenciaAparente));
				
				double fatorDePotencia = Math.cos(Math.toRadians(anguloTensao-anguloCorrente));
				textFieldFatorDePotencia.setText(String.valueOf(fatorDePotencia));
				
				
			
			}
		});
		btnSimular.setBounds(574, 502, 173, 27);
		getContentPane().add(btnSimular);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			MenuPrincipal menu;
			menu= new MenuPrincipal();
			menu.setVisible(true);
			
			}
		});
		btnVoltar.setBounds(753, 406, 91, 81);
		getContentPane().add(btnVoltar);
		
		JLabel label_3 = new JLabel("Amplitude");
		label_3.setBounds(457, 214, 70, 17);
		getContentPane().add(label_3);
		
	}
}
