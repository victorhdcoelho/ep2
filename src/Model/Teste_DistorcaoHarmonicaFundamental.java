package Model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class Teste_DistorcaoHarmonicaFundamental {

	@Test
	public void testCalcularSimulacao() {
		double amplitude = 220.0;
		double anguloDeFase=0.0;
		double w =  Math.PI*2.0*60;
		List<Double>TesteList=new ArrayList<Double>();
		for(int i=0;i<80;i++) {
		TesteList.add(amplitude*Math.cos(Math.toRadians((w*i)+anguloDeFase)));
		}
		List<Double>CompareList=new ArrayList<Double>();
		DistorcaoHarmonicaFundamental distorcaoHarmonicaFundamental= new DistorcaoHarmonicaFundamental(220.0,0.0);
		CompareList=distorcaoHarmonicaFundamental.calcularSimulacao();
		
		for(int i =0;i<80;i++) {
			assertEquals(TesteList.get(i),CompareList.get(i),0.03);
		}
		
		
		
	}

	@Test
	public void testDistorcaoHarmonicaFundamental() {
		DistorcaoHarmonicaFundamental distorcaoHarmonicaFundamental= new DistorcaoHarmonicaFundamental(220.0,0.0);
		assertEquals(distorcaoHarmonicaFundamental.amplitude,220.0,0.01);
		assertEquals(distorcaoHarmonicaFundamental.anguloDeFase,0.0,0.01);

	}

	@Test
	public void testFluxoDePotenciaMain() {
		List<Double> list = new ArrayList<Double>();
		FluxoDePotenciaMain fluxoDePotencia = new FluxoDePotenciaMain();
		list=fluxoDePotencia.calcularSimulacao();
		assertNull(list);
		
	}

}
