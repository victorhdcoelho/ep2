package Model;
import java.util.ArrayList;
import java.util.List;

public class DistorcaoHarmonicaResultados extends FluxoDePotenciaMain {

	protected List<Double>l1;
	protected List<Double>l2;
	protected List<Double>l3;
	protected List<Double>l4;
	protected List<Double>l5;
	protected List<Double>l6;
	
	public DistorcaoHarmonicaResultados(List<Double>l1,List<Double>l2,List<Double>l3,List<Double>l4,List<Double>l5,List<Double>l6){
		this.l1=l1;
		this.l2=l2;
		this.l3=l3;
		this.l4=l4;
		this.l5=l5;
		this.l6=l6;
	};
	
	public List<Double> calcularSimulacao(List<Double>fundamental){
		List<Double>resultados=new ArrayList<Double>();
		for(int i=0;i<80;i++) {
			//Serie de Furier 
			resultados.add(fundamental.get(i)+(l1.get(i)+l2.get(i)+l3.get(i)+l4.get(i)+l5.get(i)+l6.get(i)));
			
		}
		
		return resultados;
		
	}; 
	
}
