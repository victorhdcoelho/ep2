package Model;
import java.util.ArrayList;
import java.util.List;

public class FluxoDePotenciaResultado extends FluxoDePotenciaMain {
	protected double potenciaAtiva;
	protected double potenciaReativa;
	protected double fatorDePotencia;
	protected double potenciaAparente;
	protected List<Double> listCorrente;
	protected List<Double> listTensao;
	
	public FluxoDePotenciaResultado() {
	}

	public List<Double> getListCorrente() {
		return listCorrente;
	}

	public void setListCorrente(List<Double> listCorrente) {
		this.listCorrente = listCorrente;
	}

	public List<Double> getListTensao() {
		return listTensao;
	}

	public void setListTensao(List<Double> listTensao) {
		this.listTensao = listTensao;
	}
	
	public List<Double> calcularSimulacao(){
		List<Double> listResultado = new ArrayList<Double>();
		for(int i=0;i<80;i++) {
			listResultado.add(listCorrente.get(i)*listTensao.get(i));

		}
		return listResultado;
	}
}
