package Model;

import java.util.List;

public class DistorcaoHarmonicaFundamental extends FluxoDePotenciaMain {
	FluxoDePotenciaTensao distorcaoHarmonicaFundamental = new FluxoDePotenciaTensao(0.0,0.0);
	public DistorcaoHarmonicaFundamental(double amplitude, double anguloDeFase) { 
		this.amplitude=amplitude;
		this.anguloDeFase=anguloDeFase;
	}
	
	public List<Double> calcularSimulacao() {
		List<Double>fundamental;
		FluxoDePotenciaTensao distorcaoHarmonicaFundamental = new FluxoDePotenciaTensao(amplitude,anguloDeFase);
		fundamental=distorcaoHarmonicaFundamental.calcularSimulacao(0);
		return fundamental;
		
	};
	
}
