package Model;
import java.util.ArrayList;
import java.util.List;

public class DistorcaoHarmonicaHarmonicos extends FluxoDePotenciaMain {
protected double numeroHarmonico;

public DistorcaoHarmonicaHarmonicos(double amplitude, double anguloDeFase,double numeroHarmonico){
	this.numeroHarmonico=numeroHarmonico;
	this.amplitude=amplitude;
	this.anguloDeFase=anguloDeFase;
};
// Calculo dos eixos x e y para ser importada para o grafico;
public List<Double> calcularSimulacao(){
	List<Double>list=new ArrayList<Double>();
	
	for(int i=0;i<80;i++) {
		list.add(amplitude*Math.cos(Math.toRadians((numeroHarmonico*w*i)+anguloDeFase)));
	}	
	return list;
	
}
	
}
