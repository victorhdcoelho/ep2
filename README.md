<h1>EP2 - Calculadora para engenheiros</h1>
<h3>Feito por : Victor Hugo Dias Coelho</h3>

<h3>Manual de como usar</h3>
<p> Primeiramente veja se você possui o JAVA instalado em sua máquina junto com a sua JVM correspondente.</p>

<p> Após verificado se possui o java verifique também se possui uma IDE e fácil acesso a línguagem JAVA (aconselho o ECLIPSE ou NETBEENS).</p>

<p> Outro ponto importante de se reparar e se você possui o plugin para utilizar a biblioteca Swing no Windows Maker.Após a verificação podemos ir para o funcionamento do app.</p>

<p> Ao abrir a IDE de sua escolha abra a pasta do trabalho e observe o packge src aperte nele e vá até o packge controller onde ser encontra a main do programa. Aperte duas vezes na classe dentro desse pacote e execute no botão no lado superior da IDE(ECLIPSE).</p>
<img src="doc/1.png"></img>

<img src="doc/2.png"><img>

<p> Logo após executar irá aparecer a seguinte tela na sua frente.</p>

<img src="doc/3.png"></img>

<p> Escolhar a simulação que desejar. No meu caso vou mostrar o funcionamento das duas simulações na ordem que elas aparecem.</p>

<p> Primeramente coloque valores para a amplitude e o angulo de fase para a tensão desejada. Aperte no botão validar para ver se os valores vão passar pelo tratamento de erros do app</p>

<p> Segundamente coloque valores para a amplitude e o angulo de fase para a corrente desejada. Aperte no botão validar para ver se foi confirmado seus valores.</p>

<p> Logo em seguida caso os valores passados sejam validos aperte o botão simular. Logo após aperta será gerado um gráfico da potência instantânea. Além disso a simulação mostra o valor da potência Ativa, potência Reativa, potência Aparente e o fator de potência. Infelizmente, não consegui fazer o gráfico de triângulo de potência. Caso coloque os mesmo inputs que eu coloquei os seguintes valores devem aparecer junto com seus respectivos gráficos.</p>

<img src="doc/4.png"></img>
<img src="doc/5.png"></img>

<p> Caso digite valores não permitidos nos campos de solicitação de valores mensagens de erro devem aparecer. Assim como na imagem a serguir</p>

<img src="doc/6.png"></img>

<p> Caso queira fazer mais uma simulação diferente aperte no botão para voltar. E no menu principal escolha a segunda simulação.</p>

<p> A segunda simulação funciona de forma mais complexa. Primeiramente no campo de amplitude coloque a amplitude do componente fundamental e no campo a baixo coloque o angulo de fase do componente também. Ao lado do spinner de angulo de fase exite o campo de ordem que é o campo responsável por ditar quantas harmonicas irão ser liberadas para edição esse número só pode ser de 0 há 6 caso contrario o programa dará erro.</p>
<img src="doc/7.png"></img>
<p> Após colocar todas as informações e apertar no botão validar, a edição das harmonicas serão liberadas. Preenchas os campos com os valores respectivos no problema e aperte em validar. Logo depois se todos os valores foram aceitos aperte em simular onde os calculos para o funcionamento da simulação seráo chamadas e te darão os gráfico da **série** **de** **Fourier**</p>
<img src="doc/8.png"></img>
<p>No app também foram feitas o diagrama de classes e o teste da classe DistorcaoHarmonicaFundamental assim como mostrado a seguir.</p>
<img src="doc/DiagramaDeClasses.jpg"></img>
<img src="doc/9.png"></img>
